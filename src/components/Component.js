export default class Component {
	tagName;
	attribute;
	children;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}

	renderAttribute(){
		return `${this.attribute.name}="${this.attribute.value}"`;
	}

	render() {
		if(this.children==null || this.children=='undefined'){
			let res = this.renderAttribute();
			return `<${this.tagName} ${res}/>`;
		}
		else{
			return `<${this.tagName}>${this.children.join("")}</${this.tagName}>`;
		}
	}
}