import Component from "./Component.js";

export default class Img extends Component {
	url;
	constructor(url) {
		super('img', {name:'src',value:url}, null)
	}
}